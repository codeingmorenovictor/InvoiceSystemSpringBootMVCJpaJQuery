package com.invoiceSystem.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.invoiceSystem.models.Cliente;

public interface IClienteDao extends PagingAndSortingRepository<Cliente, Long>{
}