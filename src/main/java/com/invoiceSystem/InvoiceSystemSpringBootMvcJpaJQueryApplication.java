package com.invoiceSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoiceSystemSpringBootMvcJpaJQueryApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoiceSystemSpringBootMvcJpaJQueryApplication.class, args);
	}

}
